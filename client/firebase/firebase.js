import * as firebase from 'firebase';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyCX8Q5ZaullMXtpfoPRSmsqCKhE2-YNdHY",
    authDomain: "myfirebaseproject-a7d42.firebaseapp.com",
    databaseURL: "https://myfirebaseproject-a7d42.firebaseio.com",
    projectId: "myfirebaseproject-a7d42",
    storageBucket: "myfirebaseproject-a7d42.appspot.com",
    messagingSenderId: "347853818455"
  };

  firebase.initializeApp(config);

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('static/js/firebase-messaging-sw.js')
    .then(function(registration) {
        console.log('Service worker has been registered with scope: ', registration.scope);
        const messaging = firebase.messaging();
        messaging.useServiceWorker(registration);
        messaging.requestPermission()
            .then(function() {
                console.log('have permission');
                const token = messaging.getToken();
                return token;
            })
            .then(function(token) {
                createFirebaseId(token);
                console.log(token);
            })
            .catch(function(err){
                console.log('Unable to get notification token. ');
                console.log(err);
            })
        messaging.onMessage(function(payload) {
            console.log("In onMessage function: ");
            console.log("onMessage: ", payload);
        });
        messaging.setBackgroundMessageHandler(function(payload) {
            console.log('[firebase-messaging-sw.js] Received background message ', payload);
            // Customize notification here
            const notificationTitle = 'Background Message Title';
            const notificationOptions = {
              body: 'Background Message body.',
              icon: '/firebase-logo.png'
            };
          
            return self.registration.showNotification(notificationTitle,
                notificationOptions);
          });
    })
}

function createFirebaseId(deviceToken) {
    var data = {};
    data.fCMId = deviceToken;
   fetch('/api/v1/update/firebase', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  })


}