import apiClient from '../utils/apiClient';

export const setNotifications = (notifications) => ({
	type: 'SET_NOTIFICATION',
	notifications,
});

export const setAllNotificationsRead = () => ({
    type: 'MARK_ALL_READ',
});

export const startSetNotifications = () => {
	return (dispatch) => {
        return apiClient('http://localhost:9001/api/v1/get/notifications','get', null, null, {'Access-Control-Allow-Origin': '*', crossdomain: true }).then((jsonResponse) => {
            dispatch(setNotifications(jsonResponse.data));
        });
	};
};

export const markAllNotificationsRead = (id) => {
    return (dispatch) => {
        return apiClient('http://localhost:9001/api/v1/mark/notifications/read','post', null, { id }, {'Access-Control-Allow-Origin': '*', crossdomain: true }).then((jsonResponse) => {
            dispatch(setAllNotificationsRead());
        });
    }
}