import React from 'react';
import SearchExampleStandard from './SearchExampleStandard.jsx';

const SearchExampleInput = () => <SearchExampleStandard input={{ icon: 'search', iconPosition: 'left' }} />

export default SearchExampleInput