/* global contextPath */
import qs from 'qs';
import axios from 'axios';

const TIMEOUT = 600000;
function apiClient(url = '', httpMethod = 'get', params = {}, data = {}, headers = {}) {
  return axios({
    url,
    timeout: TIMEOUT,
    method: httpMethod,
    data,
    headers,
    params,
    paramsSerializer(param) {
      return qs.stringify(param, { arrayFormat: 'repeat' });
    },
  })
  .catch((error) => {
    console.log(error);
  });
}

export default apiClient;